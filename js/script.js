$(document).ready(function () {
    
    $("#gameadd").on("submit", function (e) {

        var checked = false;

        var f1 = $('#g2a');
        var f2 = $('#mmoga');
        var f3 = $('#kinguin');

        if (f1.val() != "" || f2.val() != "" || f3.val() != "") {
            checked = true;
        } else {
            f1.addClass("errorinput");
            f2.addClass("errorinput");
            f3.addClass("errorinput");

            alert("Es muss mindestens 1 Link eingetragen sein");
        }

        if (!checked) {
            e.preventDefault();
        }
        
    });

    $("form").on('click keypress','button[name="delete"]', function(e) {

        var Name = $(this).parent().find("input[type='hidden']").val()

        if(confirm("Möchten Sie wirklich das Spiel '"+ Name+ "' löschen?")){
            return true;
        }

        return false;

    });

    $("button[name='edit']").click(function(e){
        e.preventDefault();

        var h1 = $(this).closest("form").find("h1");
        var p = $(this).closest("form").find("p");
        var li = $(this).siblings("ul").find("li");
  
        if($(this).next("button").length == 0) {
            $(this).after('<button type="submit" name="save" class="btn btn-success" value="save">Save</button>');

            h1.attr("contentEditable", true);
            h1.addClass("errorinput");
            p.attr("contentEditable", true);
            p.addClass("errorinput");
            li.attr("contentEditable", true);
            li.addClass("errorinput");

        }else {
            $(this).next().remove();

            h1.attr("contentEditable", false);
            h1.removeClass("errorinput");
            p.attr("contentEditable", false);
            p.removeClass("errorinput");
            li.attr("contentEditable", false);
            li.removeClass("errorinput");
        }

    });

    $("form").keypress(function(e){ 
        return e.which != 13; 

    });
});

$(document).on('click keypress', 'button[name="save"]', function(e){ 

    var h1 = $(this).closest("form").find("h1");
    var p = $(this).closest("form").find("p");
    var li = $(this).siblings("ul").find("li").map(function(){
        if($(this).text() !== "X"){return $(this).text()}else{return ""};
    }).get().join(",");
    
    $(this).siblings("ul").after('<input type="hidden" name="newGameURL" value="'+li+'" />');
    $(this).siblings("ul").after('<input type="hidden" name="newGameBesch" value="'+p.text()+'" />');
    $(this).siblings("ul").after('<input type="hidden" name="newGameName" value="'+h1.text()+'" />');
    
});
