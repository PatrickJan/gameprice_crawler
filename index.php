<?php
session_start();
require('inc/config.php');
?>

<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <link href="css/bootstrap.min.css" rel="stylesheet">

    <script src="js/jquery-3.3.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <link href="css/style.css" rel="stylesheet">
    <script src="js/script.js"></script>

    <title>GameKey Crawler</title>
</head>

<body>
    <?php
    if (isset($_GET['url'])) {
        $url = $_GET['url'];
        
        if ($url != "settings" && $url != "cronjob") {
            redirect();
        }
    }

    if (isset($_POST["create"]) && $_POST["create"] == "submit") {
        if ($_POST["g2a"] != "" || $_POST["mmoga"] != "" || $_POST["kinguin"] != "") {
            $query = " INSERT INTO game_info (gameName, gameBesch) VALUES (:gameName, :gameBesch);";
            $query2 = " INSERT INTO game_links (id, linkMMOGA, linkG2A, linkKINGUIN) VALUES
            (:id, :MMOGA, :G2A, :KINGUIN);";
    
            $db = dbConnect();
    
            try {
                $stmt = $db->prepare($query);
                $stmt->execute(array(":gameName" => $_POST["gamename"], ":gameBesch" => $_POST["beschreibung"]));
                $id = $db->lastInsertId();
    
                $stmt = $db->prepare($query2);
                $stmt->execute(array(":id"=>$id, ":MMOGA" => $_POST["mmoga"], ":G2A" => $_POST["g2a"], ":KINGUIN" => $_POST["kinguin"]));
            } catch (Exception $e) {
                $error = customDBErrorMessage($e->getCode());
            }
        }
    }

    if (isset($_POST["save"])&& $_POST["save"] == "save") {
        if (strlen($_POST["newGameURL"]) > 0) {
            $link = explode(",", $_POST["newGameURL"]);

            getdbMessage(
                "UPDATE game_info
                LEFT JOIN game_links ON game_info.id = game_links.id
                SET game_info.gameName = :newGameName,
                    game_info.gameBesch = :newGameBesch,
                    game_links.linkG2A = :newG2A,
                    game_links.linkMMOGA = :newMMOGA,
                    game_links.linkKINGUIN = :newKINGUIN
                WHERE game_info.gameName = :Name",
                array(":newGameName" => $_POST["newGameName"],
                    ":newGameBesch" => $_POST["newGameBesch"],
                    ":newG2A" => $link[0],
                    ":newMMOGA" => $link[1],
                    ":newKINGUIN" => $link[2],
                    ":Name" => $_POST["gameName"]),
                '',
                true
            );
        }
    }

    if (isset($_POST["delete"])&& $_POST["delete"] == "delete") {
        if (isset($_POST["gameName"]) && $_POST["gameName"] != "") {
            getdbMessage("DELETE game_info, game_links, game_value FROM game_info 
            LEFT JOIN game_links ON game_info.id = game_links.id
            LEFT JOIN game_value ON game_info.id = game_value.id
            WHERE gameName = :Name", array(":Name"=>$_POST["gameName"]), '', true);
        }
    }

    if (isset($error)) {
        echo("<script>
        $(document).ready(function () { 
            alert('".customDBErrorMessage($e->getCode())."');
        })   
    </script>");
    }

    ?>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">

        <h1>
            <a href="index.php">
                <?php
                if (isset($url)) {
                    echo($name." - ".ucwords($url));
                } else {
                    echo($name);
                }
                ?>
            </a>
        </h1>

        <ul class="navbar-nav ml-auto">

            <li class="nav-item">
                <div class="dropdown">
                    <button type="button" class="btn btn-primary" id="navbarDropdown3" role="button" data-toggle="dropdown"
                        aria-haspopup="true" aria-expanded="false">
                        Add Game
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <form class="p-4" method="post" id="gameadd" action="<?php echo htmlentities($_SERVER["REQUEST_URI"]);?>">

                            <div class="form-group">
                                <label for="gamename">Game Name:</label>
                                <input type="text" name="gamename" id="gamename" class="form-control" required>
                                <label for="beschreibung">Game Info:</label>
                                <textarea name="beschreibung" id="beschreibung" rows="3" cols="30" class="form-control"></textarea>
                            </div>
                            <hr />
                            <div class="form-group">
                                <label for="g2a"><a href="https://www.g2a.com" target="_blank" rel="noreferrer" >G2A:</a></label>
                                <input type="text" name="g2a" id="g2a" class="form-control">
                                <label for="mmoga"><a href="https://www.mmoga.de" target="_blank" rel="noreferrer" >MMOGA:</a></label>
                                <input type="text" name="mmoga" id="mmoga" class="form-control">
                                <label for="kinguin"><a href="https://www.kinguin.net" target="_blank" rel="noreferrer" >KINGUIN:</a></label>
                                <input type="text" name="kinguin" id="kinguin" class="form-control">

                            </div>

                            <button type="submit" name="create" class="btn btn-success" value="submit">Create
                                a new Record</button>
                        </form>
                    </div>
                </div>
            </li>
        </ul>
    </nav>

    <main>
        
    <?php
    if (isset($url)) {
        require_once("inc/settings.php");
    } else {
        require_once("inc/list.php");
    }
    ?>

    </main>

    <footer class="container-fluid">
        <p>
            <?php
            if (isset($url)) {
                echo('<a href="index.php">back</a> - ');
            }
            ?>

            <a href="index.php?url=settings">Settings</a>
            -
            <?php
            if (isset(jsonfile()->TimeNOW)) {
                echo(jsonfile()->TimeNOW." - ");
            }
            ?>

            <?php echo($version);?>

        </p>
    </footer>

</body>

</html>