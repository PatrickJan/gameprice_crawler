<?php

$result = getdbMessage("SELECT * FROM game_info LEFT JOIN game_links ON game_info.id = game_links.id");

foreach ($result as $row) {
        echo("<section class='settings'>
        <form name='gameSettings' method='post' action='".htmlentities($_SERVER["REQUEST_URI"])."'>

        <button type='submit' name='delete' class='btn btn-danger' value='delete'>Delete</button>
        <button name='edit' class='btn btn-warning'>Edit</button>
        
        <input type='hidden' name='gameName' value='".$row["gameName"]."' />
        
        <h1>".$row["gameName"]."</h1>
        ".($row["gameBesch"] != "" ? "<p>".$row["gameBesch"]."</p>" : "<p>&nbsp;</p>")."
            <ul>
                <li>".($row["linkG2A"] != "" ? "<a href='".$row["linkG2A"]."' target='_blank' rel='noreferrer'>".$row["linkG2A"]."</a>" :
                 "X" )."</li>
                <li>".($row["linkMMOGA"] != "" ? "<a href='".$row["linkMMOGA"]."' target='_blank' rel='noreferrer'>".$row["linkMMOGA"]."</a>" :
                 "X")."</li>
                <li>".($row["linkKINGUIN"] != "" ? "<a href='".$row["linkKINGUIN"]."' target='_blank' rel='noreferrer'>".$row["linkKINGUIN"]."</a>" :
                 "X")."</li>
            </ul>
        </form>
    </section>
    ");
}

echo("<section class='double'><a href='inc/cronjob.php' target='_blank'>Cronjob</a></section>");
