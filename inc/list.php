<?php

$result = getdbMessage("SELECT gameName,gameDatum,gameBesch,preisG2A,linkG2A,preisMMOGA,linkMMOGA,preisKINGUIN,linkKINGUIN FROM `game_info` 
LEFT JOIN game_links ON game_info.id = game_links.id LEFT JOIN game_value ON game_info.id = game_value.id WHERE gameDatum = :tagheute", array(":tagheute" => $date));


foreach ($result as $row) {
    echo("<section>
        <h1>".$row["gameName"]."</h1>
        ".($row["gameBesch"] != "" ? "<p>".$row["gameBesch"]."</p>" : "<p>&nbsp;</p>")."
          <ul>
          "

        .($row["linkG2A"] != "" ?
            "   <li><a href=".$row["linkG2A"]." target='_blank'> G2A Preis: ".($row["preisG2A"] != 0 ? $row["preisG2A"]." €" : " - " )."</a></li>" :
            "   <li>G2A Preis: ".($row["preisG2A"] != 0 ? $row["preisG2A"]." €" : " - " )."</li>")
        
        .($row["linkMMOGA"] != "" ?
            "   
             <li><a href=".$row["linkMMOGA"]." target='_blank'> MMOGA Preis: ".($row["preisMMOGA"] != 0 ? $row["preisMMOGA"]." €" : " - " )."</a></li>" :
            "   
             <li>MMOGA Preis: ".($row["preisMMOGA"] != 0 ? $row["preisMMOGA"]." €" : " - " )."</li>")

        .($row["linkKINGUIN"] != "" ? 
            "   
             <li><a href=".$row["linkKINGUIN"]." target='_blank'> KINGUIN Preis: ".($row["preisKINGUIN"] != 0 ? $row["preisKINGUIN"]." €" : " - " )."</a></li>" :
            "   
             <li>KINGUIN Preis: ".($row["preisKINGUIN"] != 0 ? $row["preisKINGUIN"]." €" : " - " )."</li>").

          "
          </ul>
      </section>
    ");
}
