<?php

require("config.php");

$result = getdbMessage("SELECT game_info.id, gameName, linkMMOGA, linkG2A, linkKINGUIN FROM game_links LEFT JOIN game_info ON game_links.id = game_info.id");
$db = dbConnect();

$query = " INSERT INTO game_value (id, gameDatum, preisMMOGA, preisG2A, preisKINGUIN)  VALUES (:id, :datum, :preisMMOGA, :preisG2A, :preisKINGUIN) 
ON DUPLICATE KEY UPDATE preisMMOGA = :preisMMOGA2, preisG2A = :preisG2A2, preisKINGUIN = :preisKINGUIN2";

$stmt = $db->prepare($query);

$price1 = 0;
$price2 = 0;
$price3 = 0;

foreach ($result as $row) {
    if ($row["linkMMOGA"] != "" && (stripos($row["linkMMOGA"], "http") === 0)) {
        $price1 = getMMOGAPrice($row["linkMMOGA"]);
    }
    if ($row["linkG2A"] != "" && (stripos($row["linkG2A"], "http") === 0)) {
        $price2 = getG2APrice($row["linkG2A"]);
    }
    if ($row["linkKINGUIN"] != "" && (stripos($row["linkKINGUIN"], "http") === 0)) {
        $price3 = getKinguinPrice($row["linkKINGUIN"]);
    }

    $stmt->execute(array(":id" =>$row["id"], ":datum" => $date, ":preisMMOGA" => $price1, ":preisG2A" => $price2, ":preisKINGUIN" => $price3,
     ":preisMMOGA2" => $price1, ":preisG2A2" => $price2, ":preisKINGUIN2" => $price3));


    echo($row["gameName"]."<br />");
    flush();
    ob_flush();

    $price1 = 0;
    $price2 = 0;
    $price3 = 0;
}
jsonfile("write", array("TimeNOW"=>date("G:i d-m-Y")));

echo("<br /><br /> Done");
