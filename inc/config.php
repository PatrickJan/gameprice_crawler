<?php

$name = "GameKey Crawler";
$version = "1.0";

$date = date("Y-m-d");

/**
 * Basic DB Connection
 * @return object
 */
function dbConnect()
{
    require_once("dbData.php");

    try {
        $dbdata = dbdata();
        $db = new PDO(
            "mysql:host=".$dbdata['host'].
            ";dbname=".$dbdata['db'].
            ";charset=utf8",
            "".$dbdata['user']."",
            "".$dbdata['pass'].""
        );
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
    } catch (PDOException $e) {
        echo 'Error: ' . $e->getMessage();
    }
    return $db;
}

/**
 * Get DB Answers
 * @param string $query - the mysql command used
 * @param array $args - params in array to prepare
 * @param boolean $single - if you want just use One mysql call
 * @param boolean $noreturn - if you expect an mysql return
 * @return boolean|array
 */
function getdbMessage($query, $args = '', $single = false, $noreturn = false)
{
    $db = dbConnect();

    if ($noreturn) {
        if (is_array($args)) {
            try {
                $statement = $db->prepare($query);
                $statement->execute($args);
            } catch (Exception $e) {
                throw new Exception('0: '.$e->getMessage());
                exit;
            }
        } else {
            try {
                $statement = $db->prepare($query);
                $statement->execute();
            } catch (Exception $e) {
                throw new Exception('0: '.$e->getMessage());
                exit;
            }
        }
        return true;
    }

    if ($args == '') {
        if ($single == false) {
            try {
                $statement = $db->prepare($query);
                $statement->execute();

                $result = $statement->fetchAll(PDO::FETCH_ASSOC);

                return $result;
            } catch (Exception $e) {
                throw new Exception('1: '.$e->getMessage());
                exit;
            }
        } else {
            try {
                $statement = $db->prepare($query);
                $statement->execute();

                $result = $statement->fetch(PDO::FETCH_ASSOC);

                return true;
            } catch (Exception $e) {
                throw new Exception('2: '.$e->getMessage());
                exit;
            }
        }
    } else {
        if (is_array($args)) {
            if ($single == false) {
                try {
                    $statement = $db->prepare($query);
                    $statement->execute($args);

                    $result = $statement->fetchAll(PDO::FETCH_ASSOC);

                    return $result;
                } catch (Exception $e) {
                    throw new Exception('3: '.$e->getMessage());
                    exit;
                }
            } else {
                try {
                    $statement = $db->prepare($query);
                    $statement->execute($args);
    
                    $result = $statement->fetch(PDO::FETCH_ASSOC);
    
                    return $result;
                } catch (Exception $e) {
                    throw new Exception('4: '.$e->getMessage());
                    exit;
                }
            }
        } else {
            throw new Exception("Parameter müssen als Array übergeben werden");
            exit;
        }
    }
}

function customDBErrorMessage($errorCode = 0)
{
    $errors = [
        "23000" => "Fehler: Der Eintrag existiert schon",
        "HY000" => "Fehler: Kann nicht auf DB verbinden"
    ];

    return array_key_exists($errorCode, $errors) ? $errors[$errorCode] : "Unbekannter Fehler: ".$errorCode;
};

/**
 * @param string Subadress to redirect
 */
function redirect($url = '')
{
    if ($url == '') {
        header("Location: http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"]);
    } else {
        header("Location: http://".$_SERVER["SERVER_NAME"].$_SERVER["PHP_SELF"].$url);
    }
    exit();
}


/**
* Curl send get request, support HTTPS protocol
* @param string $url The request url
* @param string $refer The request refer
* @param int $timeout The timeout seconds
* @param array $header The other request header
* @return mixed
*/
function getRequest($url, $refer = "", $timeout = 10, $header = [])
{
    $curlObj = curl_init();
    $ssl = stripos($url, "https://") === 0 ? true : false;
    $encoding = strpos($url, "g2a.com") !== false ? true : false;

    $options = [
        CURLOPT_URL => $url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_FOLLOWLOCATION => 1,
        CURLOPT_AUTOREFERER => 1,
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36",
        CURLOPT_TIMEOUT => $timeout,
        CURLOPT_REFERER => $refer,
        CURLOPT_CUSTOMREQUEST => "GET",
        CURLOPT_HTTPHEADER => array(
            "cache-control: no-cache",
            "accept-language: de-DE,de;q=0.9,en-US;q=0.8,en;q=0.7"
          ),
    ];
    
    if ($refer) {
        $options[CURLOPT_REFERER] = $refer;
    }
    if ($ssl) {
        //support https
        $options[CURLOPT_SSL_VERIFYHOST] = false;
        $options[CURLOPT_SSL_VERIFYPEER] = false;
    }
    if ($encoding) {
        $options[CURLOPT_ENCODING] = "";
        $options[CURLOPT_COOKIEJAR] = "-";
    }

    curl_setopt_array($curlObj, $options);
    $returnData = curl_exec($curlObj);
    
    if (curl_errno($curlObj)) {
        //error message
        $returnData = curl_error($curlObj);
    }

    curl_close($curlObj);
    return $returnData;
}


/**
* DOMX get Price from Kinguin
* @param string $url The request url
* @param int $amount The amount of Price returns
* @return string
*/
function getKinguinPrice($url)
{

    $rawdata = getRequest($url);

    $doc = new DOMDocument();
    @$doc->loadHTML($rawdata, LIBXML_ERR_NONE);

    $path = new DOMXpath($doc);


    $price = $path->query("//div[@class='table-responsive physical-products-overflow-popup']
    //td[@class='offers-list__body-col offers-list__body-col--price']");
    @$price = (float)str_replace("€", "", $price[0]->textContent);

    if (!is_float($price)) {
        throw new LogicException("KINGUIN: Something went wrong");
    }
    return $price;
}


/**
* DOMX get Price from MMOGA
* @param string $url The request url
* @return string
*/
function getMMOGAPrice($url)
{
    $rawdata = getRequest($url);

    $doc = new DOMDocument();
    @$doc->loadHTML($rawdata, LIBXML_ERR_NONE);

    $path = new DOMXpath($doc);


    $price = $path->query("//div[@class='row produktdetailDown']//p[@class='now']");
    @$price = $price[0]->textContent;
    
    $price = (float) str_replace(",",".",str_replace("€", "", $price));


    if (!is_float($price)) {
        throw new LogicException("MMOGA: Something went wrong");
    }
    return $price;
}


/**
* DOMX get Price from MMOGA
* @param string $url The request url
* @return string
*/
function getG2APrice($url)
{
    $rawdata = getRequest($url);

    $doc = new DOMDocument();
    @$doc->loadHTML($rawdata, LIBXML_ERR_NONE);

    $path = new DOMXpath($doc);

    $price = $path->query("//div[@class='price-wrapper']//span[@class='price']");
    @$price = $price[0]->textContent;

    $price = (float)str_replace("EUR", "", $price);

    if (!is_float($price)) {
        throw new LogicException("G2A: Something went wrong");
    }
    return $price;
}

/**
 * Write into a JSON file
 * @param string $type - read/write file to/from
 * @param array $array - if set writes array to file
 */
function jsonfile($type = "read", $array = null)
{
    $file = 'update.json';

    if ($type == "read") {
        $fp = fopen("inc/".$file, 'r') or die("konnte datei nicht lesen");
        if ($length = filesize("inc/".$file)) {
            $fr = json_decode(fread($fp, $length));
            fclose($fp);
        
            return $fr;
        }
        return false;
    } elseif ($type == "write" && is_array($array)) {
        $fp = fopen($file, 'w') or die("konnte datei nicht lesen");
        
        fwrite($fp, json_encode($array));
        fclose($fp);

        return true;
    } else {
        return "Falsche Übergabeparameter";
        exit();
    }
}
